package com.example.lesson_8.data.`object`

data class Film(
    val id: String,
    val title: String,
    val url: String
)