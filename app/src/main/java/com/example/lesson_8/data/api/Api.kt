package com.example.lesson_8.data.api

import com.example.lesson_8.data.`object`.Film
import retrofit2.Call
import retrofit2.http.GET

interface Api {
    @GET("/films")
    fun fetchFilms(): Call<ArrayList<Film>>
}