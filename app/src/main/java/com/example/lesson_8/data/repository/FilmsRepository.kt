package com.example.lesson_8.data.repository

import android.util.Log
import com.example.lesson_8.App
import com.example.lesson_8.data.`object`.Film
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FilmsRepository {
    fun fetchFilms(onResult: (ArrayList<Film>?) -> Unit) {
        App.networkService
            .fetchFilms()
            .enqueue(object : Callback<ArrayList<Film>> {
                override fun onResponse(
                    call: Call<ArrayList<Film>>,
                    response: Response<ArrayList<Film>>
                ) {
                    onResult.invoke(response.body())
                }

                override fun onFailure(call: Call<ArrayList<Film>>, t: Throwable) {
                    Log.e("Error", t.message.toString())
                }

            })
    }
}