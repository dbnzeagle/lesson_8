package com.example.lesson_8.data.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiService {

    private val myClient: OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(20, TimeUnit.SECONDS)
        .build()

    private fun provideGson(): Gson = GsonBuilder()
        .setLenient()
        .create()

    private fun retrofit(): Retrofit = Retrofit.Builder()
        .client(myClient)
        .baseUrl("https://ghibliapi.herokuapp.com")
        .addConverterFactory(GsonConverterFactory.create(provideGson()))
        .build()

    fun networkService(): Api = retrofit().create(Api::class.java)
}