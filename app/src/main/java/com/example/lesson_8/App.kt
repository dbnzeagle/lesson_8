package com.example.lesson_8

import android.app.Application
import com.example.lesson_8.data.api.Api
import com.example.lesson_8.data.api.ApiService
import com.example.lesson_8.data.repository.FilmsRepository

class App : Application() {
    companion object {
        lateinit var networkService: Api
        lateinit var repository: FilmsRepository

        fun initNetwork(network: Api) {
            networkService = network
        }

        fun initRepository(rep: FilmsRepository) {
            repository = rep
        }
    }

    override fun onCreate() {
        super.onCreate()
        initNetwork(ApiService().networkService())
        initRepository(FilmsRepository())
    }
}