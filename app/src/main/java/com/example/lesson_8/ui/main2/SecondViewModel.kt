package com.example.lesson_8.ui.main2

import android.graphics.Color
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.lesson_8.App
import com.example.lesson_8.data.`object`.Film
import java.util.*
import kotlin.collections.ArrayList

class SecondViewModel : ViewModel() {

    val bgLiveData = MutableLiveData<Int>()

    val listData = MutableLiveData<ArrayList<Film>>()

    init {
        fetchData()
    }

    fun fetchData() {
        App.repository.fetchFilms {
            if (it != null) {
                listData.postValue(it)
            } else {
                Log.i("NUll", "null")
            }
        }
    }

    fun onItemClick(item: String) {
        val newString = item + "какая та строчка"
    }

    fun generateRandomColor() {
        val random = Random()
        val color = Color.rgb(
            random.nextInt(256),
            random.nextInt(256),
            random.nextInt(256)
        )
        bgLiveData.postValue(color)
    }


}