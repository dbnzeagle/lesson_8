package com.example.lesson_8.ui.main2

import android.os.Bundle
import android.widget.Button
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.lesson_8.R
import com.example.lesson_8.ui.main.CustomAdapter

class SecondActivity : AppCompatActivity() {

    private val secondViewModel: SecondViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
    }

    private fun initViews() {
        findViewById<Button>(R.id.main_button).setOnClickListener {
            secondViewModel.generateRandomColor()
        }

        secondViewModel.bgLiveData.observe(this, { color ->
            findViewById<ConstraintLayout>(R.id.main_root)
                .setBackgroundColor(color)
        })

        secondViewModel.listData.observe(this, { item ->
            val recyclerView = findViewById<RecyclerView>(R.id.main_list)
            recyclerView.layoutManager = LinearLayoutManager(this)
            recyclerView.adapter = CustomAdapter(item) {}
        })
    }


}