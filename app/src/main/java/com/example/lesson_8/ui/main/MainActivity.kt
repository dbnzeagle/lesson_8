package com.example.lesson_8.ui.main

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.lesson_8.R
import com.example.lesson_8.data.`object`.Film
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity(), IMainView {

    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initPresenter()
        initViews()
        presenter.init()
    }

    private fun initViews() {
        findViewById<Button>(R.id.main_button).setOnClickListener {
            presenter.generateRandomColor()
        }
    }

    private fun initPresenter() {
        presenter = MainPresenter(this)
    }

    override fun showInfo(item: ArrayList<Film>) {
        val recyclerView = findViewById<RecyclerView>(R.id.main_list)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = CustomAdapter(item) {}
    }

    override fun changeBgColor(color: Int) {
        findViewById<ConstraintLayout>(R.id.main_root)
            .setBackgroundColor(color)
    }

    override fun initInfo(item: String) {
        Snackbar.make(
            findViewById(R.id.main_root),
            item,
            Snackbar.LENGTH_LONG
        )

        Toast.makeText(this, item, Toast.LENGTH_LONG).show()
        Log.i("TAg", item)
    }
}