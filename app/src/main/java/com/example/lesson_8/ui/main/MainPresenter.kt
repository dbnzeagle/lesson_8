package com.example.lesson_8.ui.main

import android.graphics.Color
import android.util.Log
import com.example.lesson_8.App
import java.util.*

class MainPresenter(private val view: IMainView) : IMainPresenter {

    override fun init() {
        view.initInfo("Презентер создан")
        App.repository.fetchFilms {
            if (it != null) {
                view.showInfo(it)
            } else {
                Log.i("NUll", "null")
            }
        }
    }

    override fun destroy() {
        Log.i("Presenter", "Презентер ушел")
    }

    override fun onItemClick(item: String) {
        val newString = item + "какая та строчка"
    }

    override fun generateRandomColor() {
        val random = Random()
        val color = Color.rgb(
            random.nextInt(256),
            random.nextInt(256),
            random.nextInt(256)
        )
        view.changeBgColor(color)
    }
}