package com.example.lesson_8.ui.main

import com.example.lesson_8.data.`object`.Film

interface IMainView {
    fun showInfo(item: ArrayList<Film>)
    fun changeBgColor(color:Int)
    fun initInfo(item: String)
}

interface IMainPresenter {
    fun init()
    fun destroy()
    fun generateRandomColor()
    fun onItemClick(item: String)
}