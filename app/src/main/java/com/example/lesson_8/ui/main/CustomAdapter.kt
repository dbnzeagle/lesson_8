package com.example.lesson_8.ui.main

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.lesson_8.R
import com.example.lesson_8.data.`object`.Film
import java.util.*
import kotlin.collections.ArrayList

class CustomAdapter(private val list: ArrayList<Film>, private val onClick: (Int) -> Unit) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_list, parent, false
        )
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val random = Random()
        holder.text.text = list[position].title
        holder.view.setBackgroundColor(
            Color.rgb(
                random.nextInt(256),
                random.nextInt(256),
                random.nextInt(256)
            )
        )
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val view = view.findViewById<CardView>(R.id.item_list_card)
        val text = view.findViewById<TextView>(R.id.item_list_text)

    }
}
